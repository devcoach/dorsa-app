# Dorsa Calculator Project

Behold My Awesome Project!

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: MIT


### Setup Project

Create virtual env:

    $ python -m venv .venv
    $ source .venv/bin/activate
    $ pip install -r requirements/local.txt
    $ pip install -r requirements/local.txt

Create database :

    $ sudo -u postgres createuser myuser;
    $ alter user myuser with encrypted password 'mypass';
    $ create database dorsadb;

Import database :

    $ psql -U myuser -h localhost -W -d dorsadb -f dorsadb.sql

Set Project default setting enviroment :

     $ export DATABASE_URL=postgres://postgres:<password>@127.0.0.1:5432/dorsadb
     $ export REDIS_URL=redis://127.0.0.1:6379
     $ export USE_DOCKER=NO

> If you import database this step is not needed


Create admin user and get token database :

    $ python manage.py createsuperuser
    $ curl -P {{baseUrl}}/api/v1/auth-token/


Export database :

    $ pg_dump -U myuser -h localhost -F p -d dorsadb > dorsadb.sql


You see histoy call

![history_home](docs/history-home.png?raw=true)
![hisotry show](./docs/history-home.png?raw=true)


## Deployment

The following details how to deploy this application.

### Docker

See detailed [cookiecutter-django Docker documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html).
