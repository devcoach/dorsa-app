from django.contrib import admin
from calculator.models import History, ApiRequestLimit

# Register your models here.

from django.shortcuts import render, redirect
from django.contrib import messages

from django.core.cache import cache


@admin.register(History)
class CallHistoryAdmin(admin.ModelAdmin):
    list_display = ("id", "a", "b", "ip", "created_at")

    actions = [
        "unblock_user_ip"
    ]

    def unblock_user_ip(self, request, queryset):
        items = queryset.all()
        for history in items:
            cache.delete(f"calculator_success_{history.ip}")
            cache.delete(f"calculator_wrong_{history.ip}")
        self.message_user(request, f"Users has been unblocked.", messages.SUCCESS)
        return redirect('.')

    unblock_user_ip.short_description = "Remove Blocked Users"


@admin.register(ApiRequestLimit)
class ApiRequestLimitAdmin(admin.ModelAdmin):
    list_display = ("id", "api")

# @admin.register(RequestLimit)
# class RequestLimitAdmin(admin.ModelAdmin):
#     list_display = ("id", "api_request", "success_count", "failed_count")
