from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import TotalView, CalcSumView, CalHistoryView

urlpatterns = {
    path("sum/", CalcSumView.as_view(), name="call-sum"),
    path("history/", CalHistoryView.as_view(), name="call-history"),
    path("total/", TotalView.as_view(), name="total"),
}

urlpatterns = format_suffix_patterns(urlpatterns)
