from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from calculator.models import History
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny
from calculator.serializers import HistorySerializer
from django.db.models import F, Sum
from dorsa_project.utils.ip import get_client_ip


@permission_classes((AllowAny,))
class CalcSumView(APIView):

    def get(self, request):
        a = request.GET.get("a", "")
        b = request.GET.get("b", "")

        if not a.isnumeric() or not b.isnumeric():
            return Response({
                "success": False,
                "message": "Please enter valid value a or b !"
            }, status=status.HTTP_400_BAD_REQUEST)

        History.objects.create(a=a, b=b, ip=get_client_ip(request))
        return Response({
            "success": True,
            "result": a + b
        }, status=status.HTTP_200_OK)


class CalHistoryView(APIView):

    def get(self, request):
        histories = History.objects.all().order_by('-id')
        serializer = HistorySerializer(histories, many=True)
        return Response({
            "total": serializer.data,
            "success": True
        })


class TotalView(APIView):

    def get(self, request):
        total = History.objects.aggregate(total=Sum(F('a') + F('b')))['total']

        return Response({
            "total": total,
            "success": True
        })
