from django.db import models


class ApiRequestLimit(models.Model):
    api = models.CharField(max_length=100, blank=True, null=True)
    per_success_hour = models.IntegerField(blank=True, null=True)
    per_wrong_hour = models.IntegerField(blank=True, null=True)


class History(models.Model):
    a = models.IntegerField(blank=True, null=True)
    b = models.IntegerField(blank=True, null=True)
    # api_request = models.ForeignKey(ApiRequestLimit, on_delete=models.CASCADE, blank=True, null=True)
    ip = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

# class RequestLimit(models.Model):
#     api_request = models.ForeignKey(ApiRequestLimit, on_delete=models.CASCADE, blank=True, null=True)
#     ip = models.CharField(max_length=100, blank=True, null=True)
#     success_count = models.IntegerField(blank=True, null=True)
#     failed_count = models.IntegerField(blank=True, null=True)
#     created_at = models.DateTimeField(auto_now_add=True)
