from rest_framework import serializers
from calculator.models import History


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ["a", "b"]
