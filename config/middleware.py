from django.core.cache import cache
from dorsa_project.utils.ip import get_client_ip

from calculator.models import ApiRequestLimit

from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer


class CalculatorThrottleMiddleware:
    '''
     Check user request
    '''

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.path.startswith("/api/v1/calculator/sum"):
            ip = get_client_ip(request)
            timeout = 60 * 60
            if cache.get(f"calculator_success_{ip}", None) is None:
                api_limit = ApiRequestLimit.objects.filter(api=request.path).last()
                cache.set(f"calculator_success_{ip}", api_limit.per_success_hour, timeout=timeout)
                cache.set(f"calculator_wrong_{ip}", api_limit.per_wrong_hour, timeout=timeout)
            if cache.get(f"calculator_success_{ip}") <= 0:
                ttl = cache.ttl(f"calculator_success_{ip}")
                resp = Response(
                    data={"data": f"Request was blocked. Expected available in {ttl} seconds.", 'success': False},
                    status=429
                )
                resp.accepted_renderer = JSONRenderer()
                resp.accepted_media_type = "application/json"
                resp.renderer_context = {}
                resp.render()
                return resp
            if cache.get(f"calculator_wrong_{ip}") <= 0:
                ttl = cache.ttl(f"calculator_wrong_{ip}")
                resp = Response(
                    data={"data": f"Request was blocked. Expected available in {ttl} seconds.", 'success': False},
                    status=429
                )
                resp.accepted_renderer = JSONRenderer()
                resp.accepted_media_type = "application/json"
                resp.renderer_context = {}
                resp.render()
                return resp

            response = self.get_response(request)
            # cache.get(f"calculator_{ip}", None)
            print(response.status_code)
            if response.status_code == 200:
                cache.decr(f"calculator_success_{ip}")
            if response.status_code in [400, 405]:
                cache.decr(f"calculator_wrong_{ip}")

        response = self.get_response(request)

        return response
