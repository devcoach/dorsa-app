from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter
from django.urls import include, path

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

# router.register("users", UserViewSet)
urlpatterns = [
    path("calculator/", include(("calculator.urls", "calculator")))
]

app_name = "api_v1"
urlpatterns += router.urls

app_name = "api_v1"
urlpatterns += router.urls
