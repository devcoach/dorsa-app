from django.urls import path

from dorsa_project.users.api.views import GetTokenView

app_name = "users"
urlpatterns = [
    path("token/", GetTokenView.as_view(), name="token"),
]
